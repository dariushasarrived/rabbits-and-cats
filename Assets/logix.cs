using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class logix : MonoBehaviour
{
    /*My dear friends, if you want to add some more cats or rabbits in this game, just duplicate it(right mouse
    button on rabbit or cat, and press duplicate, then just place it on scene. If you want to increase speed of your rabbits and cats, there are script, that is attached to the main camera
    that called logix. There you can see a field named speed. you can increase it or decrease. P.S make sure speed value > 0 otherwise rabbits and cats wont be able to move
    Thats all*/
    [SerializeField] GameObject ball;
    [SerializeField] float speed;
    
    
    creatures creat = new creatures();
    GameObject[] rabbits;
    GameObject[] cats;
    List<Vector3> rabbits_start_pos = new List<Vector3>();
    List<Vector3> cats_start_pos = new List<Vector3>();
    Vector3 ball_start_pos;
    private void Awake()
    {
        ball_start_pos = ball.transform.position;
        rabbits = GameObject.FindGameObjectsWithTag("rabbit");
        cats = GameObject.FindGameObjectsWithTag("cat");
        foreach (GameObject rabbit in rabbits)
        {
            rabbits_start_pos.Add(rabbit.transform.position);
        }
        foreach (GameObject cat in cats)
        {
            cats_start_pos.Add(cat.transform.position);
        }
    }
    void Start()
    {
         
    }
   
    void creatures_advanced_logix()
    {
        //here is logic for rabbit chasing the ball
        foreach(GameObject rab in rabbits)
        {
            creat.creature_move(rab, ball, speed);
            rab.transform.rotation = new Quaternion(0, 0, 0, 0);

        }
        //here is logic for cat chasing the ball
        foreach (GameObject cat in cats)
        {
            creat.creature_move(cat, ball, speed);
            cat.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        //here is logic for reseting the game
        if (ball.transform.position.y <= -0.1f)
        {
            reset_the_game();
        }


    }
    void reset_the_game()
    {
        ball.transform.position = ball_start_pos;
        for(int i =0; i<rabbits.Length; i++)
        {
            rabbits[i].transform.position = rabbits_start_pos[i];
        }
        for (int i = 0; i < cats.Length; i++)
        {
            cats[i].transform.position = cats_start_pos[i];
        }
    }
    private void FixedUpdate()
    {
        creatures_advanced_logix();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private class creatures
    {

        internal void creature_move(GameObject creature, GameObject ball, float step)
        {
            creature.transform.position = Vector3.MoveTowards(creature.transform.position, ball.transform.position, step * Time.deltaTime);
        }
        internal void creature_stop(GameObject creature)
        {
            creature.transform.Translate(0, 0, 0);
        }
    }
  

}
